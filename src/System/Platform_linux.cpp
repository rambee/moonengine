#include "System/Platform.h"
#include <stdio.h>
#include <stdlib.h>

using namespace Moon;

void Platform::canExit()
{
    return true;
}

void Platform::isVsync()
{
    return true;
}

void Platform::print(std::string text)
{
    printf(text.c_str());
}

void Platform::println(std::string text)
{
    text.append("\n");
    printf(text.c_str());
}
