#ifndef PLATFORM_H
#define PLATFORM_H

#include <string>

namespace Moon {

class Platform {

public:
    static bool isVsync();

    static void print(std::string text);

    static void println(std::string text);

    static bool canExit();

private:


};

}

#endif // PLATFORM_H
