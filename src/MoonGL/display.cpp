#include "display.h"
using namespace GL;

Display::Display()
{
}

bool Display::init()
{
    // Initialise GLFW
    if(!glfwInit())
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return false;
    }
    // Ensure we can capture the escape key being pressed below
    glfwEnable( GLFW_STICKY_KEYS );

    // Dark blue background
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
}

void Display::swapBuffers()
{
    glfwSwapBuffers();
}

void Display::readSettings()
{
    if(!Resize)
        glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE,GL_TRUE);

    glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}

void Display::setResolution(int height, int width)
{
}

void Display::setTitle(std::string title)
{
    Title = title;
    setTitle();
}

void Display::setTitle()
{
    glfwSetWindowTitle(Title.c_str());
}

std::string Display::getTitle()
{
    return Title;
}

void Display::setResize(bool value)
{
    Resize = value;
}

bool Display::getResize()
{
    return Resize;
}

void Display::setFullscreen(bool value)
{
    Fullscreen = value;
}

bool Display::getFullscreen()
{
    return Fullscreen;
}

void Display::close()
{
    glfwCloseWindow();
}

void Display::terminate()
{
    glfwTerminate();
}

bool Display::open()
{
    readSettings();

    int mode;
    if(getFullscreen())
        mode = GLFW_FULLSCREEN;
    else
        mode = GLFW_WINDOW;

    // Open a window and create its OpenGL context
    if( !glfwOpenWindow(height, width, 0,0,0,0, 32,0, mode ) )
    {
        fprintf( stderr, "Failed to open window. If you have an Intel GPU, they are not 3.3 compatible.\n" );
        terminate();
        return false;
    }
    setTitle();
    // Initialize GLEW
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        terminate();
        return false;
    }

    return true;
}

bool Display::isOpened()
{
    return glfwGetWindowParam(GLFW_OPENED);
}

inline float Display::getAspectRatio() const
{
    return (float)width/(float)height;
}


