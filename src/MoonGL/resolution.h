#ifndef RESOLUTION_H
#define RESOLUTION_H

class Resolution
{
public:
    Resolution(int height, int width);
    Resolution(const Resolution &other);
    int getHeight() const;
    int getWidth() const;

    void setHeight(int height);
    void setWidth(int width);

    bool operator==(const Resolution &other) const;
    inline bool operator!=(const Resolution &other) const { return !operator==(other); }

private:
    int height = 0;
    int width = 0;
};

#endif // RESOLUTION_H
