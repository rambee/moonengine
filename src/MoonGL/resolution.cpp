#include "resolution.h"

Resolution::Resolution(int height, int width)
{
    this->height = height;
    this->width = width;
}

Resolution::Resolution(const Resolution &other)
{
    height = (other.getHeight());
    width = (other.getWidth());
}

int Resolution::getHeight() const
{
    return height;
}

int Resolution::getWidth() const
{
    return width;
}

void Resolution::setHeight(int height)
{
    this->height = height;
}

void Resolution::setWidth(int width)
{
    this->width = width;
}

bool Resolution::operator ==(const Resolution &other) const
{
    if(getHeight() == other.getHeight() && getWidth() == other.getWidth())
        return true;
    else
        return false;
}
