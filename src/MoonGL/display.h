#ifndef DISPLAY_H
#define DISPLAY_H

#include <Base.h>
#include <MoonGL/resolution.h>

#include <GL/glew.h>
#include <GL/glfw.h>
#include <glm/glm.hpp>
using namespace glm;

namespace GL {
class Display
{
public:
    Display();

    bool init();

    void swapBuffers();

    bool open();
    void close();
    void terminate();

    bool isOpened();

    void setTitle(std::string title);
    void setTitle();

    std::string getTitle();

    void setResolution(int height, int width);

    void setResize(bool value);
    bool getResize();

    void setFullscreen(bool value);
    bool getFullscreen();

    /**
     * Gets the aspect ratio of the window. (width / height)
     *
     * @return The aspect ratio of the window.
     */
    inline float getAspectRatio() const;

private:
    void readSettings();

    int height = 800;
    int width = 600;

    Resolution *resolution = new Resolution(800,600);

    bool Fullscreen = false;
    bool Resize = false;

    std::string Title = "OpenGL Window";
};
}

#endif // DISPLAY_H
