#include "fastmath.h"

using namespace Moon;

FastMath::FastMath()
{
}

float FastMath::deg_to_rad(float x)
{
   return (x) * 0.0174532925f;
}

float FastMath::rad_ro_deg(float x)
{
   return (x) * 57.29577951f;
}

int FastMath::random(int max)
{
    return rand()/max;
}

float FastMath::Pi()
{
    return 3.14159265358979323846f;
}
