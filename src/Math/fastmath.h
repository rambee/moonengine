#ifndef FASTMATH_H
#define FASTMATH_H

#include "Base.h"

namespace Moon {
class FastMath
{
public:
    FastMath();

    static float deg_to_rad(float x);

    static float rad_ro_deg(float x);

    static int random(int max);

    static float Pi();
};
}
#endif // FASTMATH_H
