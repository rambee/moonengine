#include "application.h"
#include <MoonGL/resolution.h>

using namespace Moon;
static Application *appInstance = NULL;

Application::Application()
{
    display = new GL::Display();
    appInstance = this;

    if(!display->init()) {
        return;
    }
}

bool Application::isRunning()
{
    return Running;
}

void Application::open()
{
    if(!display->open()) {
        finalize();
        return;
    }
    render();
}

void Application::render()
{
    Running = true;
    do {
        if(glfwGetKey( GLFW_KEY_ESC ) == GLFW_PRESS)
            Running = false;
        // Draw nothing

        // Swap buffers
        swapBuffers();

    } // Check if the ESC key was pressed or the window was closed
    while(isRunning() && display->isOpened());

    // Close OpenGL window and terminate Display
    finalize();
    terminate();

    return;
}

void Application::println(std::string text)
{
    text.append("\n");
    printf(text.c_str());
}

void Application::setResolution(int height, int width)
{

}

void Application::setTitle(std::string title)
{
    APP_TITLE = title;
    setTitle();
}

void Application::setTitle()
{
    display->setTitle(APP_TITLE);
}

std::string Application::getTitle() const
{
    return APP_TITLE;
}

bool Application::getResize() const
{
    return display->getResize();
}

bool Application::getFullscreen()
{
    return display->getFullscreen();
}

void Application::setFullscreen(bool value)
{
    display->setFullscreen(value);
}

void Application::setResize(bool value)
{
    display->setResize(value);
}

Application *Application::getInstance()
{
    return appInstance;
}

void Application::swapBuffers()
{
    display->swapBuffers();
}

void Application::terminate()
{
    display->terminate();
}
