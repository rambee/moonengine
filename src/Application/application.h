#ifndef APPLICATION_H
#define APPLICATION_H

#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <GL/glew.h>

#include <GL/glfw.h>

#include <glm/glm.hpp>
#include <MoonGL/display.h>
using namespace glm;

namespace Moon {

class Application
{
public:
    Application();

    bool isRunning();
    /**
     * @brief Set window title
     * @param Window title
     */
    void setTitle(std::string title);
    void setTitle();
    std::string getTitle() const;

    bool getResize() const;
    void setResize(bool value);

    bool getFullscreen();
    void setFullscreen(bool value);

    /**
    * Gets the single instance of the application.
    *
    * @return The single instance of the application.
    */
    static Application* getInstance();

    static void println(std::string text);

    void setResolution(int height, int width);

    /**
    * Finalize callback that is called when the game on exits.
    */
    virtual void finalize() = 0;

    void open();
    void render();
    void terminate();


private:
    GL::Display *display;

    bool Running = false;

    void swapBuffers();

    std::string APP_TITLE = "Moon Engine Application";

    void readSettings();
};
}

#endif // APPLICATION_H
