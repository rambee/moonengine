#ifndef PAPPLICATION_H
#define PAPPLICATION_H

#include "Application/application.h"
using namespace Moon;

class PApplication : public Application
{
public:
    PApplication();
    virtual void finalize();
};

#endif // PAPPLICATION_H
