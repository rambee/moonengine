#include <stdio.h>
#include <stdlib.h>

#include <Application/application.h>
#include <Math/fastmath.h>
#include <playground/papplication.h>
#include <json/json.h>
using namespace Moon;

int main( void )
{
    PApplication *app = new PApplication();
    app->setTitle("Demo");
    app->setFullscreen(false);
    app->setResize(true);
    app->setResolution(800,600);

    app->open();

	return 0;
}

