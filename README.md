Moon Game Engine
================

Cross-platform Game engine written in C++

### Features

* Cross-platform  (Linux, Windows, Mac OS X)
* OpenGL
* Physics
* Audio
